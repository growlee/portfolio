const fs = require('fs-extra')
const fm = require('front-matter')
const path = require('path')
const glob = require('glob')
const uuid = require('uuid-by-string')
const Liquid = require('liquidjs').Liquid

const util = require('./util')

class Klay {
	
	root = process.cwd()

	klay = {
		root: '.',
		source: './site',
		output: './dist',
		assets: {
			allow: ['.gif','.png','.svg','.jpg','.jpeg'],
			files: []
		},
		content: {
			allow: ['.md'],
			files: []
		},
		templates: {
			allow: ['.liquid'],
			files: []
		}
	}

	constructor () {
		this.klay.root   = this.root
		this.klay.source = path.resolve(this.root, this.klay.source)
		this.klay.output = path.resolve(this.root, this.klay.output)
		
		this.siteWarmup()
	}

	async siteWarmup () {
		this.klay.content.files = await this.siteContent()

		this.compileContent()
	}

	siteContent () {
		const root = path.join(this.klay.source, 'content')
		return new Promise((resolve, reject) => {
			const files = glob.sync(path.join(root, '**', '*')).filter(file => {
				const extension = path.extname(file)
				if (extension && this.klay.content.allow.includes(extension)) {
					return file
				}
			})
			return files.length ? resolve(files) : reject()
		})
		.then(files => {
			return files.map(file => {
				// create a flat file route
				const route = file
					.replace(root, '')
					.replace(/\/\d+-/g, '/')
					.replace('/index.md', '')
					.replace('.md', '')
				// check for parent
				const parent = !['.','/'].includes(path.dirname(route)) ? path.dirname(route) : ''
				// check if filename begins with _ it wont be available by route
				const hidden = util.startsWith(path.basename(file), '_')
				// parse front matter
				const { body, attributes } = fm(fs.readFileSync(file).toString())
				// get file meta data
				const { birthtimeMs, mtimeMs } = fs.statSync(file)
				// protect constants from front matter attributes
				delete attributes['_source']
				delete attributes['_output']
				delete attributes['_hidden']
				delete attributes['_created']
				delete attributes['_updated']
				// return usable file data
				return {
					// system properties
					_id: uuid(file).split('-', 1)[0],
					_slug: file.replace(root, '').replace(/^\/|\/index.md$|.md$/gi, '').replace(/\//g, '_'),
					_source: file,
					_output: !hidden ? file.replace(root, this.klay.output).replace(/\/\d+-/g, '/').replace('.md', '.html') : null,
					_hidden: hidden,
					_created: birthtimeMs,
					_updated: mtimeMs,
					_expires: '',
					_children: [],
					// render properties
					_layout: 'layouts/default',
					_template: 'default',
					// dynamic properties
					route: hidden ? '' : route || '/',
					parent,
					content: body,
					...attributes,
				}
			})
		})
	}

	compileContent () {
		const files = this.klay.content.files

		const engine = new Liquid({
			root: path.join(this.klay.source, 'templates'),
			extname: this.klay.templates.allow[0]
		})

		files.forEach(async file => {
			if (file._output) {
				const root = path.dirname(file._output)
				const html = await engine.renderFile(file._template, {
					klay: {
						assets: this.klay.assets.files,
						content: this.klay.content.files
					},
					...file
				})
				await fs.ensureDir(root)
				await fs.writeFile(file._output, html)
			}
		})
	}
}

new Klay()
