module.exports = {
	startsWith: function (haystack, needle) {
		const startsWith = new RegExp(`^${needle}`)
		return startsWith.test(haystack) ? true : false
	}
}
